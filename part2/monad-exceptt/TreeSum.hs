-- task https://stepik.org/lesson/38580/step/13?unit=20505
module TreeSum where

import Control.Monad.Trans.Except
import Control.Monad.Trans.Writer
import Data.Monoid
import Data.Foldable

data ReadError = EmptyInput | NoParse String
  deriving Show

data Tree a = Leaf a | Fork (Tree a) a (Tree a)

tryRead :: (Read a, Monad m) => String -> ExceptT ReadError m a
tryRead [] = throwE EmptyInput
tryRead s =
    case reads s of
        [(a,b)] -> if null b then return a
                                      else throwE $ NoParse s
        _       -> throwE $ NoParse s


treeSum t = let (err, s) = runWriter . runExceptT $ traverse_ go t
            in (maybeErr err, getSum s)
  where
    maybeErr :: Either ReadError () -> Maybe ReadError
    maybeErr = either Just (const Nothing)

go :: String -> ExceptT ReadError (Writer (Sum Integer)) ()
go = tryRead

-- todo: finish it