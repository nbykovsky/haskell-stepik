-- task https://stepik.org/lesson/38580/step/8?unit=20505
module AskPassword where

import Control.Monad.Trans.Except
import Control.Monad.IO.Class (liftIO)
import Data.Foldable (msum)
import Control.Monad.Trans.Maybe
import Data.Char (isNumber, isPunctuation)
import Control.Monad.Trans
import Control.Applicative
import Control.Monad (when)

newtype PwdError = PwdError String

type PwdErrorIOMonad = ExceptT PwdError IO

instance Monoid PwdError where
    mempty = PwdError ""
    mappend (PwdError a) (PwdError b) = PwdError $ a ++ b

askPassword :: PwdErrorIOMonad ()
askPassword = do
  liftIO $ putStrLn "Enter your new password:"
  value <- msum $ repeat getValidPassword
  liftIO $ putStrLn "Storing in database..."

getValidPassword :: PwdErrorIOMonad String
getValidPassword = do
  s <- liftIO getLine
  catchE (validate s) (\(PwdError a) -> do liftIO $ putStrLn a; throwE $ PwdError a)

validate :: String -> PwdErrorIOMonad String
validate s = do
  when (length s < 8) $ throwE $ PwdError "Incorrect input: password is too short!"
  when (not $ any isNumber s) $ throwE $ PwdError "Incorrect input: password must contain some digits!"
  when (not $ any isPunctuation s) $ throwE $ PwdError "Incorrect input: password must contain some punctuation!"
  return s