module TryRead where

import Control.Monad.Trans.Except

data ReadError = EmptyInput | NoParse String
  deriving Show

tryRead :: (Read a, Monad m) => String -> ExceptT ReadError m a
tryRead [] = throwE EmptyInput
tryRead s =
    case reads s of
        [(a,b)] -> if null b then return a
                                      else throwE $ NoParse s
        _       -> throwE $ NoParse s
