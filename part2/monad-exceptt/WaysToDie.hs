module WaysToDie where

data Tile = Floor | Chasm | Snake
  deriving Show

data DeathReason = Fallen | Poisoned
  deriving (Eq, Show)

type Point = (Integer, Integer)
type GameMap = Point -> Tile


moves :: GameMap -> Int -> Point -> [Either DeathReason Point]
moves m n p =
    case m p of
        Chasm -> [Left Fallen]
        Snake -> [Left Poisoned]
        Floor | n == 0 -> [Right p]
        Floor ->  moves m (n-1) (x-1, y) ++ moves m (n-1) (x+1, y) ++ moves m (n-1) (x, y-1) ++ moves m (n-1) (x, y+1)
    where
        x = fst p
        y = snd p

waysToDie :: DeathReason -> GameMap -> Int -> Point -> Int
waysToDie d m n p = length $ filter (flt d) (moves m n p) where
    flt :: DeathReason -> Either DeathReason Point -> Bool
    flt Fallen (Left Fallen) = True
    flt Poisoned (Left Poisoned) = True
    flt _ _ = False
