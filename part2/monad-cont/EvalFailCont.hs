-- task https://stepik.org/lesson/30723/step/10?unit=11811
module EvalFailCont where

import Control.Monad.Trans.Except
import Control.Monad


data ReadError = EmptyInput | NoParse String
  deriving Show

tryRead :: Read a => String -> Except ReadError a
tryRead [] = throwE EmptyInput
tryRead s =
    case reads s of
        [(a,b)] -> if null b then return a
                                      else throwE $ NoParse s
        _       -> throwE $ NoParse s


add :: Int -> Int -> FailCont r e Int
add x y = FailCont $ \ok _ -> ok $ x + y

addInts :: String -> String -> FailCont r ReadError Int
addInts s1 s2 = do
  i1 <- toFailCont $ tryRead s1
  i2 <- toFailCont $ tryRead s2
  return $ i1 + i2

-- solution
newtype FailCont r e a = FailCont { runFailCont :: (a -> r) -> (e -> r) -> r }

instance Functor (FailCont r e) where
    fmap = liftM

instance Applicative (FailCont r e) where
    pure = return
    (<*>) = ap


instance Monad (FailCont r e) where
    return x = FailCont $ \ok _ -> ok x

    v >>= k = FailCont $ \ok err -> runFailCont v (\a' -> runFailCont (k a') ok err) err


toFailCont :: Except e a -> FailCont r e a
toFailCont ex = case runExcept ex of
    Left e -> FailCont $ \_ err -> err e
    Right a -> FailCont $ \ok _ -> ok a

evalFailCont :: FailCont (Either e a) e a -> Either e a
evalFailCont (FailCont fc) = fc Right Left


