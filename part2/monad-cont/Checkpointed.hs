-- task https://stepik.org/lesson/30723/step/9?unit=11811
module Checkpointed where

import Control.Monad.Cont


type Checkpointed a = (a -> Cont a a) -> Cont a a

runCheckpointed :: (a -> Bool) -> Checkpointed a -> a
runCheckpointed p c =  (runCont $ c (\a' -> cont (\v -> if p (v a') then v a' else a'))) id

--runCheckpointed :: (a -> Bool) -> Checkpointed a -> a
--runCheckpointed p c = runCont (c checkpoint) id where
--    checkpoint x = Cont inner where
--        inner c =  if p (c x) then c x else x


addTens :: Int -> Checkpointed Int
addTens x1 = \checkpoint -> do
  checkpoint x1
  let x2 = x1 + 10
  checkpoint x2     {- x2 = x1 + 10 -}
  let x3 = x2 + 10
  checkpoint x3     {- x3 = x1 + 20 -}
  let x4 = x3 + 10
  return x4         {- x4 = x1 + 30 -}
