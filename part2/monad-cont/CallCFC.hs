--task https://stepik.org/lesson/30723/step/13?unit=11811
module CallCFC where

newtype FailCont r e a = FailCont { runFailCont :: (a -> r) -> (e -> r) -> r }

callCFC :: ((a -> FailCont r e b) -> FailCont r e a) -> FailCont r e a
callCFC f = FailCont $ \ok err -> runFailCont (f $ \a -> FailCont $ \_ _ -> ok a) ok err
