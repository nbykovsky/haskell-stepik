module ShowCont where

import Control.Monad.Cont

showCont :: Show a => Cont String a -> String
showCont m = runCont m show
