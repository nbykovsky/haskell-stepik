### Monad Cont
#### Continuation passing style

usual functions
```
square :: Int -> Int
square x = x^2

add :: Int -> Int -> Int
add x y = x + y
```
CPS functions:
```
square :: Int -> (Int -> r) -> r
square x c = c $ x^2

add :: Int -> Int -> (Int -> r) -> r 
add x y c = c $ x + y

demo> square 2 id
4
demo> square 2 show
"4"
demo> add 2 3 print
5
demo> square 2 square id
16
demo> square 2 (add 3) (add 5) id
12
```
We pass call back (continuation) into the function which will be invoked with function result


```
sumSquares :: Int -> Int -> (Int -> r) -> r
sumSquares x y c = 
    square x $ \x2 ->
    square y $ \y2 ->
    add x2 y2 $ \ss ->
    c ss

demo> sumSquares 3 4 show
"25"
```

#### Monad Cont
```
newtype Cont r a = Cont {runCont :: (a -> r) -> r}

-- evalCont is like runCont but with built in id
evalCont :: Cont r r -> r
evalCont m = runCont m id
```

if we had monad we would be able to write the following
```
square' :: Int -> Cont r Int
square' x = return $ x^2

add' :: Int -> Int -> Cont r Int
add' x y = return $ x + y

demo> runCont (square' 2) show
"4"
demo> evalCont (square' 2)
4
demo> evalCont (add' 3 4)
7
demo> evalCont (square' 2 >>= (add' 3) >>= (add' 5))
12
```
Sum squares in do notation:
```haskell
sumSquares' :: Int -> Int -> Cont r Int
sumSquares' x y = do
    x2 <- square' x
    y2 <- square' y
    ss <- add' x2 y2
    return ss
    
demo> evalCont (sumSquares' 3 4)
25
```

Monad Cont implementation
```haskell
instance Monad (Cont r) where
    return :: a -> Cont r a
    return x = Cont $ \c -> c x
    
    (>>=) :: Cont r a -> (a -> Cont r b) -> Cont r b
    v >>= k = Cont $ \c -> runCont v (\a -> runCont (k a) c)

-- bind could be rewritten as following (for understanding):
bind :: ((a -> r) -> r) -> (a -> (b -> r) -> r) -> (b -> r) -> r
bind v k = \c -> v (\a -> k a c) 

```

example of usage Cont
```haskell
sumIt :: Cont r Integer
sumIt = do
    a <- return 3
    b <- return 5
    return $ a + b

demo> runCont sumIt id
8

-- rewrite second return
sumIt :: Cont r Integer
sumIt = do
    a <- return 3
    b <- Cont $ \c -> c 5
    return $ a + b

demo> runCont sumIt id
8

-- continuation could be changed
sumIt :: Cont String Integer
sumIt = do
    a <- return 3
    b <- Cont $ \c -> "STOP"
    return $ a + b

demo> runCont sumIt show
"STOP"

-- continuation could be used several times
sumIt :: Cont [r] Integer
sumIt = do
    a <- return 3
    b <- Cont $ \c -> c 4 ++ c 5
    return $ a + b
    
demo> runCont sumIt show
"78"
```

#### callCC
call with current continuation 

Cont monad allows immediately terminate calculation
```haskell
test :: Integer -> Cont Integer Integer
test x = do
    a <- return 3 
    Cont $ \c -> if x > 100 then 42 else c ()
    return $ a + x
``` 
    
another option
```haskell
test' :: Integer -> Cont r Integer
test' x = callCC $ \k -> do
    a <- return 3
    when (x > 100) k 42
    return $ a + x
    
-- callCC :: ((Integer -> Cont r b) -> Cont r Integer) -> Cont r Integer
```
In the example above function k has type `Integer -> Cont r b` and terminates calculation right after it has been called

callCC implementation
```haskell
callCC :: ((a -> Cont r b) -> Cont r a) -> Cont r a
callCC f = Cont $ \c -> runCont (f $ \a -> Cont $ \_ -> c a) c

-- replace Cont to it's structure
callCC' :: ((a -> (b -> r) -> r) -> (a -> r) -> r) -> (a -> r) -> r
callCC' f = \c -> f (\a _ -> c a) c

```