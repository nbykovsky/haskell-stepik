### Alternative and MonadPlus type classes

Definition of Alternative
```haskell
class Applicative f => Altenrnative f where
    empty :: f a
    (<|>) :: f a -> f a -> f a
```

Definition of Monad Plus
```haskell
class (Alternative m, Monad m) => MonadPlus m where
    mzero :: m a
    mzero = empty
    mplus :: m a -> m a -> m a
    mplus = <|>
```

Alternative for Maybe
```haskell
instance Alternative Maybe where
    empty = Nothing
    Nothing <|> r = r
    l       <|> _ = l
```

Monad Plus for Maybe
Maybe has everything what is required for Monad Plus hence the minimal definition is the following:  
```haskell
instance MonadPlus Maybe
```

<|> takes the first non-Nothing element.:
```haskell
demo> Nothing <|> Just 3 <|> Just 5 <|> Nothing
Just 3
```
mplus does the same:
```haskell
demo> Nothing `mplus` Just 3 `mplus` Just 5 `mplus` Nothing
Just 3
```

Alternative for List
```haskell
instanse Alternative [] where
    empty = []
    (<|>) = (++)
```
Alternative operator (<|>) for List behaves like concatenation:
```haskell
demo> [1, 2, 3] <|> [4, 5]
[1, 2, 3, 4, 5]
```
Cut Applicative operator (*>) for List behaves like multiplication (preserves the number of effects)
```haskell
demo> [1, 2, 3] *> [4, 5]
[4, 5, 4, 5, 4, 5]
```

Hence Monad Plus for List could be created using default implementations
```haskell
instance MonadPlus []
```
```haskell
demo> [1, 2, 3] `mplus` [4, 5]
[1, 2, 3, 4, 5]
```

multiplication for List could be achieved using cut bind operator (>>) 
```haskell
demo> [1, 2, 3] >> [4, 5]
[4, 5, 4, 5, 4, 5]
```
which is equal to
```
demo> [1, 2, 3] >>= \_ -> [4, 5]
[4, 5, 4, 5, 4, 5]
```
In the modern Haskell Applicative is ancestor of Monad Plus and (*>) operator is equal to (>>) operator. It was different in the older versions haskell
 
#### Laws for MonadPlus
1. Left Zero  
    ```
    mzero >>= k == mzero
    ```

2. Right Zero  

    ```
    v >> mzero == mzero
    ```
    usually MonadPlus has 2 additional laws (one of these 2 laws must be satisfied)
3. Left Distribution   
    ```
    (a `mplus` b) >>= k == (a >>= k) `mplus` (b >>= k)
    ```
4. Left Catch law   
    ```
    return a `mplus` b == return a
    ```
    
Left catch law doesn't work for Lists:
```
demo> return 2 `mplus` [1, 2, 3]
[2, 1, 2, 3]
```
however left catch law works for Maybe
```
demo> return 2 `mplus` Just 3
Just 2
```
Actually MonadPlus represent two type classes: 
1.  all the laws + left distribution 
2.  all the laws + left catch law

#### Useful functions of MonadPlus

```
guard :: Alternative f => Bool -> f ()
guard True = pure ()
guard False = Empty
```
guard checks the condition, if it's False calculations terminate, if it's True - continue
```
pythags = do
    z <- [1..]
    x <- [1..z]
    y <- [x..z]
    guard (x^2 + y^2 == z^2)
    return (x, y, z)
```

Functions with monoidal nature  

mconcat just concatenates any monoid
```
mconcat :: Monoid m => [m] -> m 
mconcat = foldr mappend empty
```

fold concatenates any monoid in foldable container 
```
-- possible implementation
fold :: (Foldable t, Monoid m) => t m -> m
fold = foldr mappend mempty
```

```
msum :: (Foldable t, MonadPlus m) => t (m a) -> m a
-- msum = foldr mplus mzero
-- now it's replased by
msum = asum
```

```
asum :: (Foldable, Alternative f) => t (f a) -> f a
asum = foldr (<|>) empty
```

for maybe msum returns first not null element
```
demo> msum [Nothing, Just 3, Just 5, Nothing]
Just 3
```

asum does the same (more general function)
```
demo> asum [Nothing, Just 3, Just 5, Nothing]
Just 3
```

Another useful task of MonadPlus is filtering of monadic container based on predicate
```
mfilter :: MonadPlus m => (a -> Bool) -> m a -> ma
mfilter p ma = do
    a <- ma
    if p a
    then return a
    else mzero
```

```
demo> mfilter (>3) (Just 4)
Just 4
demo> mfilter (>3) (Just 2)
Nothing
```
mfilter cannot be lifted to alternative because mfilter changes structure of container.