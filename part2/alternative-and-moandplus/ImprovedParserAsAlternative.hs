-- Task https://stepik.org/lesson/30721/step/9?unit=11244

module ImprovedParserAsAlternative where

import Control.Applicative
import Control.Arrow

-- make the following parser instance of alternative
newtype PrsEP a = PrsEP { runPrsEP :: Int -> String -> (Int, Either String (a, String)) }

parseEP :: PrsEP a -> String -> Either String (a, String)
parseEP p  = snd . runPrsEP p 0

-- Solution
instance Functor PrsEP where
  f `fmap` (PrsEP p) = PrsEP $ ((<$>) (first f <$>) .) . p

instance Applicative PrsEP where
  pure x = PrsEP $ \i s -> (i, Right (x, s))
  pf <*> px = PrsEP $ \i s -> case runPrsEP pf i s of
                (i', Left e) -> (i', Left e)
                (i', Right (f, s')) -> runPrsEP (f <$> px) i' s'

instance Alternative PrsEP where
    empty = PrsEP fun where
        fun i _ = (i, Left ("pos " ++ show i ++ ": empty alternative"))
    p <|> q = PrsEP fun where
        fun i s = case runPrsEP p i s of
            (i', Right x) -> (i', Right x)
            (i', Left y) -> case runPrsEP q i s of
                (i'', Right x') -> (i'', Right x')
                (i'', Left y') -> if i'' > i' then (i'', Left y') else (i', Left y)

