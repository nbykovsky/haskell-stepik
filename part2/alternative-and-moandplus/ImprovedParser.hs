-- Task https://stepik.org/lesson/30721/step/7?unit=11244

module ImprovedParser where
import Control.Applicative

newtype PrsEP a = PrsEP { runPrsEP :: Int -> String -> (Int, Either String (a, String)) }

parseEP :: PrsEP a -> String -> Either String (a, String)
parseEP p  = snd . runPrsEP p 0

-- charEP c = satisfyEP (== c)

-- Solution
satisfyEP :: (Char -> Bool) -> PrsEP Char
satisfyEP pr = PrsEP f where
    f i "" = (succ i, Left ("pos " ++ (show $ succ i) ++ ": unexpected end of input"))
    f i (c:cs) | pr c = (i + 1, Right (c, cs))
               | otherwise = (succ i, Left ("pos " ++ (show $ succ i) ++ ": unexpected " ++ [c]))

