### [Task](https://stepik.org/lesson/30721/step/5?unit=11244)
Are the following distribution laws work for the standard implementations of Applicative, Alternative, Monad and MonadPlus of the Maybe type type class:  
```
1. (u <|> v) <*> w       =    u <*> w <|> v <*> w

2. (u `mplus` v) >>= k   =    (u >>= k) `mplus` (v >>= k)
```

### Solution

I. These are standard implementations of Applicative and Alternative for Maybe:  
```
instance Applicative Maybe where
    pure         = Just
    (Just f) <*> (Just x) = Just (f x)
    _        <*>  _       = Nothing
    
instance Alternative Maybe where
    empty = Nothing
    Nothing <|> r = r
    l       <|> _ = l
```

Case 1. `w == Nothing`:   
Left hand side  
```
`(u <|> v) <*> w == Nothing
```

Right hand side   
```
u <*> w == Nothing
v <*> w == Nothing
u <*> w <|> v <*> w == Nothing
```
Both sides are equal to Nothing

Case 2. `u == v == Nothing`:   
Left hand side   
```
(u <|> v) == Nothing
(u <|> v) <*> w == Nothing
```

Right hand side   
```
u <*> w == Nothing
v <*> w == Nothing
u <*> w <|> v <*> w == Nothing
```
Again both sides are equal to Nothing

Case 3. `u == Just f; w == Just x`:
Left hand side   
```
(u <|> v) == Just f
(u <|> v) <*> w == Just f x
```
Right hand side   
```
u <*> w == Just f x
u <*> w <|> v <*> w == Just f x
```
Both sides are equal to Just f x  

Case 4. `u == Nothing; v = Just f; w == Just x`  
Left hand side
```
(u <|> v) == Just x
(u <|> v) <*> w == Just f x
```
Right hand side   
```
u <*> w == Nothing
v <*> w == Just f x
u <*> w <|> v <*> w == Just f x
```
Both sides are equal to Just f x


II. Counter example:   
```
u = Just 5  
v = Just 10  
k 5 = Nothing  
k x = Just x * 10  
  
(u `mplus` v) >>= k == Nothing  
(u >>= k) `mplus` (v >>= k) = Just 100  
```
