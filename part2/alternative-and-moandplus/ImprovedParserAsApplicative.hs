-- Task https://stepik.org/lesson/30721/step/8?unit=11244

module ImprovedParserAsApplicative where

-- Make the following parser instance of Functor and Applicative
newtype PrsEP a = PrsEP { runPrsEP :: Int -> String -> (Int, Either String (a, String)) }

parseEP :: PrsEP a -> String -> Either String (a, String)
parseEP p  = snd . runPrsEP p 0

-- Solution
instance Functor PrsEP where
    fmap f p = PrsEP fun where
        fun i s = (fst r, h)
            where r = runPrsEP p i s
                  h = do
                     (a, s') <- snd r
                     return (f a, s')

-- more elegant solution in ImprovedParserAsAlternative.hs
instance Applicative PrsEP where
    pure a = PrsEP fun where
        fun i s = (i, Right (a, s))
    pf <*> pv = PrsEP fun where
        fun i s = (i'', res)
            where
                  (i', r1) = runPrsEP pf i s
                  (i'', r2) = case r1 of
                    Left x -> (i', Left x)
                    Right (_, s') -> runPrsEP pv i' s'
                  res = do
                    (f, _) <- r1
                    (v, s'') <- r2
                    return (f v, s'')
