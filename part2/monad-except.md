### Monad Except

```
newtype Except e a = Except {runExcept :: Either e a}
                     deriving Show
                     
-- to prevent importing except
except :: Either e a -> Except e a
except = Except
```
In the modern haskell to define monad we need to make type class functor and applicative functor
Hopefully we have standard actions for that
```
-- e is error type
instance Functor (Except e) where
    fmap = liftM
    
instance Applicative (Except e) where
    pure = return
    (<*>) = ap
    
```
we have to make type class monad later
```
instance Monad (Except e) where
 -- return :: a -> Except e a
    return a = Except (Right a)
 -- (>>=) :: Except e a -> (a -> Except e b) -> Except e b
    m >>= k = 
        case runExcept m of
            Left e -> Except (Left e)
            Right x -> k x
```

throw and catch - standard interfaces for working with Except  
throwE is used inside monadic calculations
```
throwE :: e -> Except e a
throw = except . Left
```

catchE is used after monadic calculation
```
catchE :: Except e a -> (e -> Except e' a) -> Except e' a
-- h - error handler
m `catchE` h =
    case runExcept m of
        Left e -> h e
        Right r -> except (Right r)
```

Usage  
```
do {action1; action2; action3} `catchE` handler
```
Law for Except:  
`catchE h (throwE) == h e`

##### Example

```
-- errors
data DivByError = ErrZero String | ErrOther deriving (Eq, Show)

(/?) :: Double -> Double -> Except DivByError Double
x /? 0 = throwE $ ErrZero (show x ++ "/0;")
x /? y = return $ x / y

example0 :: Double -> Double -> Except DivByError String
example0 x y = action `catchE` handler where
    action = do
        q <- x /? y
        return $ show q
    handler = \err -> return  $ show err
```

```
demo> runExcept $ example0 5 2
Right "2.5"
demo> runExcept $ example0 5 0
Right "ErrZero \"5.0/0;\""
```

##### Except as MonadPlus
Except could be declared as MonadPlus. Alternative operator will try all the calculations and return the first successful, accumulating errors

```
instance Monoid e => Alternative (Except e) where
    empty = mzero
    (<|>) = mplus
    
instance Monoid e => MonadPlus (Except e) where
    mzero = Except (Left mempty)
    Except x `mplus` Except y = Except $
        case x of
            Left e -> either (Left . mappend e) Right y
            r      -> r
```

Function either takes Either argument and applied first or second function depending on Either value
```
either :: (a -> c) -> (b -> c) -> Either a b -> c
```

Example of monoid from some exception
```
instance Monoid DivByError where
    mempty = ErrOther
    ErrZero s1 `mappend` ErrZero s2 = ErrZero $ s1 ++ s2
    ErrZero s1 `mappend` ErrOther   = ErrZero s1
    ErrOther   `mappend` ErrZero s2 = ErrZero s2
    ErrOther   `mappend` ErrOther   = ErrOther

```

Example of using error as monoid
```
example2 :: Double -> Double -> Except DivByError String
example2 x y = action `catchE` handler where
    action = do
        q <- x /? y
        guard $ y >= 0 -- guard returns mempty which is ErrOther
        return $ show q
    handler (ErrZero s) = return s
    handler ErrOther    = return "NONNEGATIVE GUARD"
```

```
demo> runExcept $ example2 5 2
Right "2.5"
demo> runExcept $ example2 5 0
Right "5.0/0;"
demo> runExcept $ example2 5 (-3)
Right "NONNEGATIVE GUARD"
```

In monad plus Errors could be accumulated using msum
```
demo> runExcept $ msum [5/?0, 7/?0, 2/?0]
Left (ErrZero "<All Errors>")
demo> runExcept $ msum [5/?0, 7/?2, 2/?0]
Right 3.5
```