### Monad WriterT

```haskell
{-# LANGUAGE InstanceSigs #-}
module WriterT where
imort Control.Applicative (liftA2)
import Data.Tuple (swap)

newtype Writer w a = Writer {runWriter :: (a, w)}

newtype WriterT w m a = WriterT {runWriterT :: m (a, w)}

demo> runWriterT (WriterT $ Just (42, "Hello!"))
Just (42, "Hello!")
```

writer - universal constructor
```haskell
writer :: Monad m => (a, w) -> Writer w m a
writer = WriterT . return 

demo> runWriter (writer (42, "Hello!")) :: [(Int, String)]
[(42, "Hello!")]
```

execWriterT - returns log and ignores value
```haskell
execWriterT :: Monad m => WriterT w m a -> m w
execWriterT = fmap snd . runWriterT

```

WriterT as Functor
```haskell
instance Functor (Writer w) where
    fmap :: (a -> b) -> Writer w a -> Writer w b
    fmap f = Writer . updater . runWriter
        where updater ~(x, log) = (f x, log)

instance Functor m => Functor (WriterT w m) where
    fmap :: (a -> b) -> WriterT w m a -> WriterT w m b
    fmap f = WriterT . fmap updater .runWriterT
        where updater ~(x, log) = (f x, log)

-- "~" - means that pattern is lazy, it will always match
demo> runWriter (fmap (^2) $ Writer (3, "A"))
(9, "A")
demo> runWriter (fmap (^2) $ Writer [(3, "A"), (4, "B")])
[(9, "A"), (16, "B")]
```


WriterT as Applicative
```haskell
instance Monoid w => Applicative (Writer w) where
    pure :: a -> Writer w a 
    pure x = Writer (x, mempty)
    (<*>) :: Writer w (a -> b) -> Writer w a -> Writer w b
    f <*> v = Writer $ updater (runWriter f) (runWriter v)
        where updater ~(g, w) ~(x, w') = (g x, w `mappend` w')
        
 instance (Monoid w, Applicative m) => Applicative (WriteT w m) where
    pure :: a -> WriterT w m a
    pure x = WriterT $ pure (x, mempty)
    (<*>) :: WriterT w m (a -> b) -> WriterT w m a -> WriterT w m b
    f <*> v = WriterT $ liftA2 updater (runWriterT f) (runWriterT v)
        where updater ~(g, w) ~(x, w') = (g x, w `mappend` w')

demo> runWriter (Writer ((*6), "Hello ") <*> Writer(7, "world!"))
(42, "Hello world!")
demo> runWriterT (writer ((*6), "Hello ") <*> writer (7, "world!")) :: [(Int, String)]
[(42, "Hello world!")]

```

WriterT as Monad
```haskell
instance Monoid w => Monad (Writer w) where
    (>>=) :: Writer w a -> (a -> Writer w b) -> Writer w b
    m >>= k = Writer $ let
        (v, w) = runWriter m
        (v', w') = runWriter (k v)
        in (v', w `mappend` w')
        
instance (Monoid w, Monad m) => Monad (WriterT w m) where
    (>>=) :: WriterT w m a -> (a -> WriterT w m b) -> WriterT w m b
    m >>= k = WriterT $ do
        ~(v, w) <- runWriterT m
        ~(v', w') <- runWriterT (k v)
        return (v', w `mappend` w')
    fail :: String Writer w m a 
    -- internal monad might know how to deal with errors
    fail = WriterT . fail

demo> runWriter $ do {x <- Writer (41, "Hello"); return (succ x)}
(42, "Hello")
demo> runWriterT $ do {x <- writer (41, "Hello"); return (succ x)} :: [(Int, String)]
[(42, "Hello")]

```

WriterT as MonadTrans
```haskell
class MonadTrans t where
    lift :: Monad m => m a -> t m a
    
instance (Monoid w) => MonadTrans (WriterT w) where
    lift :: Monad m => m a -> WriterT w m a
    lift m = WriterT $ do
        x <- m
        return (x, mempty)
```

Standard interface for WriterT

tell
```haskell
tell :: w -> Writer w ()
tell w = Writer ((), w)

demo> runWriter (tell "Hello")
((), "Hello")

tell:: Monad m => w -> Writer w m ()
tell = writer ((), w)

```

listen - allows to access log
```haskell
listen :: Monad m => WriterT w m a -> WriterT w m (a, w)
listen m = WriterT $ do
    ~(a, w) <- runWriterT m
    return ((a, w), w)

demo> wl3 = WriterT $ [(1, "one"), (10, "ten"), (20, "twenty")]
demo> runWriterT ( listen wl3)
[((1, "one"), "one"), ...]
```

censor - allows to modify log
```haskell
censor :: Monad m => (w -> w) -> Writer w m a -> Writer w m a
censor f m = WriterT $ do
    ~ (a, w) <- runWriterT m
    return (a, f w)

demo> runWriterT (censor (\(w:ws) -> [w]) wl3)
[(1, "o"), (10, "t"), (20, "t")]

```

final version of logFirstAndRetSecond
```haskell
logFirstAndRetSecond :: ReaderT [String]
                        (WriterT String Identity)
                        String
logFirstAndRetSecond = do
    el1 <- asks head
    el2 <- asks (map toUpper . head . tail)
    list $ tell el1
    return el2

demo> let strings = ["abc", "def", "hij"]
demo> runIdentity $ runWriterT (runReaderT logFirstAndRetSecond strings)
("DEFG", "abc")

```