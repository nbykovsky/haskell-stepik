### Monad ReaderT

Handmade readerT
```haskell
newtype Reader r a = Reader {runReader :: r -> a}

newtype ReaderT r m a = ReaderT {runReaderT :: r -> m a}

```
#### constructor for ReaderT
```haskell
demo>runReader (Reader (*3)) 7
21
```
ReaderT required internal monad and could be used in the following way
```haskell
demo> runReaderT (ReaderT (\x -> [x*3])) 7
[21]
demo> runReaderT (ReaderT (\x -> Just x*3)) 7
Just 21
```
reader is universal constructor irrespective on internal monad

```haskell
reader :: Monad m => (r -> a) -> ReaderT r m a
reader f = ReaderT (return . f)

demo> unReaderT (reader (3)) 7 :: [Int]
[21]
demo> unReaderT (reader (3)) 7 :: Maybe Int
Just 21
demo> demo> unReaderT (reader (3)) 7 :: IO
21
```

Let's instantiate Reader and ReaderT as Functor
```haskell
{-# LANGUAGE InstanceSigs #-}
module Demo where

instance Functor (Reader r) where
    fmap :: (a -> b) -> Reader r a -> Reader r b
    fmap f rdr = Reader $ f . runReader rdr

demo> (fmap succ $ Reader (*3)) 7
22

```
`{-# LANGUAGE InstanceSigs #-}` - extension which allows to write type signature in type class instance

```haskell
instnce Functor m => Functor (ReaderT r m) where
    fmap :: (a -> b) -> ReaderT r m a -> Reader r m b
    fmap f rdr = ReaderT $ fmap f . runReaderT rdr
    
demo> runReaderT (fmap succ $ reader (*3)) 7 :: [Int]
[22]
demo> runReaderT (fmap succ $ reader (*3)) 7 :: IO Int
22

-- complex internal monad
demo> rl3 = ReaderT $ \env -> [42, env, env*2]
demo> runReaderT (fmap succ rl3) 7 
[43, 8, 15]
```

Applicative for Reader
```haskell
instance Applicative (Reader r) where
    pure :: a -> Reader r a
    pure = Reader . const
    (<*>) :: Reader r (a -> b) -> Reader r a -> Reader r b
    f <*> v = Reader $ \env -> runReader f env (runReader v env)

demo> runReader (Reader (+) <*> Reader (^2)) 7
56
```

Applicative for ReaderT
```haskell
instance Applicative m => Applicative (ReaderT r m) where
    pure :: a -> ReaderT r m a
    pure = ReaderT . const . pure
    (<*>) :: ReaderT r m (a -> b) -> ReaderT r m a -> ReaderT r m b
    f <*> v = ReaderT $ \env -> runReaderT f env <*> runReaderT v env

demo> lef rl2 = ReaderT $ \env -> [const env, (+env)]
demo> lef rl3 = ReaderT $ \env -> [42, env, env*2]
demo> runReader (rl2 <*> rl3) 7
[7,7,7,49.14,21]
```

From other side ReaderT is a composition of 2 Applicative Functors. 
Implementation using property above
```haskell
demo> :t liftA2
liftA2 :: Applicatie f => (a -> b -> c) -> f a -> f b -> f c
demo> :t liftA2 (<*>)
liftA2 (<*>)
    :: (Applicative f1, Applicative f2) =>
       f (f1 (a -> b)) -> f (f1 a) -> f (f1 b)

-- (r -> m (a -> b)) -> (r -> m a) -> (r -> m b)
f <*> g = ReaderT $ liftA2 (<*>) (runReaderT f) (runReaderT v)

liftA2 f m1 m2 = f <$> m1 <*> m2

``` 

Lets make ReaderT monad
```haskell
instance Monad (Reader r) where 
    return = pure
    (>>=) :: Reader r a -> (a -> Reader r b) -> Reader r b
    m >>= k = Reader $ \env ->
        let v = runReader m env
        in runReader (k v) env

instance Monad m => Monad (ReaderT r m) where    
    return = pure
    (>>=) :: ReaderT r m a -> (a -> ReaderT r m b) -> ReaderT r m b
    m >>= k = ReaderT $ \env -> do
        v <- runReaderT m env
        runReader (k v) env

demo> let rl3 = ReaderT $ \env -> [42, env, env*2]
demo> runReaderT (do {x <-rl3; return (succ x)}) 7
[43,8,15]

```

lift signature
```haskell
-- parameter t - transformer
class MonadTrans t where
    lift :: Monad m => m a -> t m a
```
lift resides in Control.Monad.Trans.Class  
laws for lift
* lift . return = return
* lift (m >>= k) = lift m >>= (lift . k)  

lift - natural transformation for monad

lift for ReaderT
```haskell
instance MonadTrans (ReaderT r) where
    lift :: Monad m => m a -> ReaderT r m a
    lift m = ReaderT $ \_ -> m

demo> let rl3 = ReaderT $ \env -> [42, env, env*2]
demo> runReaderT (do {x <- rl3; y <- lift (replicate 3 10); return (x + y)}) 7
[52,52,52,17,17,17,24,24,24]
```

Reader interface

ask
```haskell
ask :: Reader r r
ask = Reader id

ask :: Monad m => ReaderT r m r
ask = ReaderT return

demo> runReaderT (do {e <- ask;f <- lift [pred, succ]; return (f e)}) 7
[6, 8]
```

asks
```haskell
asks :: (r -> a) -> Reader r a
asks f = Reader f

asks :: Monad m => (r -> a) -> ReaderT r m a
asks f = ReaderT $ return . f

demo> runReaderT (do {e <- asks (^2);f <- lift [pred, succ]; return (f e)}) 7
[48, 50]

```

local - transforms environment locally
```haskell
local :: (r -> r) -> Reader r a -> Reader r a
local f rdr = Reader $ \e -> runReader rdr (f e)

local :: (r -> r) -> ReaderT r m a -> ReaderT r m a
local f rdr = ReaderT $ runReaderT rdr . f

demo> runReaderT (do {xmod <- local (^2) rl3; e <- ask; return (xmod + e)}) 7
[49, 56, 105]

```