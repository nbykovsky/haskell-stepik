-- task https://stepik.org/lesson/38579/step/14?unit=20504
module Tree where

import Control.Monad.Trans.State
import Control.Monad.Trans.Writer
import Control.Monad.Trans
import Data.Monoid

data Tree a = Leaf a | Fork (Tree a) a (Tree a) deriving Show

numberAndCount :: Tree () -> (Tree Integer, Integer)
numberAndCount t = getSum <$> runWriter (evalStateT (go t) 1)
  where
    go :: Tree () -> StateT Integer (Writer (Sum Integer)) (Tree Integer)
    go (Leaf _) = do
        s <- get
        put $ s + 1
        lift $ tell 1
        return (Leaf s)
    go (Fork l _ r) = do
        l' <- go l
        s <- get
        put $ s + 1
        r' <- go r
        return (Fork l' s r')
