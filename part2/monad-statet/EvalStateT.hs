-- task https://stepik.org/lesson/38579/step/4?unit=20504
module EvalStateT where

newtype StateT s m a = StateT { runStateT :: s -> m (a,s) }

state :: Monad m => (s -> (a, s)) -> StateT s m a
state f = StateT (return . f)

evalStateT :: Monad m => StateT s m a -> s -> m a
evalStateT st s= do
    (a, _) <- runStateT st s
    return a

execStateT :: Monad m => StateT s m a -> s -> m s
execStateT st s= do
    (_, s') <- runStateT st s
    return s'