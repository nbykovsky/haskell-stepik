-- task https://stepik.org/lesson/38579/step/5?unit=20504
module ReaderToStateT where

import Control.Monad.Trans.Reader
import Control.Monad.Trans.State

readerToStateT :: Monad m => ReaderT r m a -> StateT r m a
readerToStateT rdr = StateT $ \e -> do--state $ \e -> (runReaderT rdr e, e)
    a <- runReaderT rdr e
    return (a, e)