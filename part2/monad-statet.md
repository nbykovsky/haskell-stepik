### Monad StateT

```haskell
newtype State s a = State {runState :: s -> (a, s)}

newtype StateT s m a = StateT { runStateT :: s -> m (a,s) }

demo> let sm = StateT $\st -> Just (st+3,*2)
demo> runStateT sm 5
Just (8, 10)
demo> let sl3 = StateT $ \st -> [(st+1, 42), (st+2, st), (st+3, st*2)]
demo> runStateT sl3 5
[(6, 42), (7,5),(8,10)]
```

universal constructor
```haskell
state :: Monad m => (s -> (a, s)) -> StateT s m a
state f = StateT (return . f)

demo> runStateT (state (\s -> ("Hello", state * 2))) 5 :: [(String, Int)]
[("Hello", 10)]
```

StateT as Functor
```haskell
instance Functor (State s) where
    fmap :: (a -> b) -> State s a -> State s b
    fmap f m = State $ \st -> updater $ runState m st
        where updater ~(x, s) = (f x, s)

instance Functor m => Functor (StateT s m) where
  fmap :: (a -> b) -> StateT s m a -> StateT s m b
  fmap f m = StateT $ \st -> fmap updater $ runStateT m st
    where updater ~(x, s) = (f x, s)

demo> runStateT (fmap (^2) sl3) 5
[(36, 42), ...]

```

StateT as Applicative
```haskell
instance Applicative (State s) where
    pure :: a -> State s a
    pure x = State $ \s -> (x, s)
    (<*>) :: State s (a -> b) -> State s a -> State s b
    f <*> v = State $ \s -> 
        let (g, s') = runState f s
            (x, s'') = runState v s'
        in (g x, s'')


-- we require Monad context because Applicative is not sufficient
instance Monad m => Applicative (StateT s m) where
  pure :: a -> StateT s m a
  pure x = StateT $ \ s -> return (x, s)

  (<*>) :: StateT s m (a -> b) -> StateT s m a -> StateT s m b
  f <*> v = StateT $ \ s -> do
      ~(g, s') <- runStateT f s
      ~(x, s'') <- runStateT v s'
      return (g x, s'')
demo> let stT1 = state $ \x -> ((^2), x + 2)
demo> let stT2 = state $ \x -> (x, x+3)
demo>runStateT (stT1 <*> stT2) 5 :: Maybe (Int, Int)
Just (49, 10)

```

StateT as Monad
```haskell

instance Monad (State s) where
    (>>=) :: State s a -> (a -> State s b) -> State s b
    m >>= k = State $ \s ->
        let (x, s') = runState m s
        in runState (k x) s'

instance Monad m => Monad (StateT s m) where
  (>>=) :: StateT s m a -> (a -> StateT s m b) -> StateT s m b
  m >>= k  = StateT $ \s -> do
    ~(x, s') <- runStateT m s
    runStateT (k x) s'

demo> runStateT $  do {g<-stT1; x<-stT2;return (g x)} 5 :: [(Int, Int)]
[(49, 10)]

```

lift function
```haskell

instance MonadTrans (StateT s) where
  lift :: Monad m => m a -> StateT s m a
  lift m = StateT $ \st -> do
    a <- m
    return (a, st)


get :: Monad m => StateT s m s
get = state $ \s -> (s, s)

put :: Monad m => s -> StateT s m ()
put s = state $ \_ -> ((), s)

modify :: Monad m => (s -> s) -> StateT s m ()
modify f = state $ \s -> ((), f s)
```