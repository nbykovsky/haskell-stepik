-- task https://stepik.org/lesson/30722/step/13?unit=11809
module SimpleError where

import Control.Monad.Trans.Except

data ListIndexError = ErrIndexTooLarge Int | ErrNegativeIndex
  deriving (Eq, Show)

newtype SimpleError = Simple { getSimple :: String }
  deriving (Eq, Show)

instance Monoid SimpleError where
    mempty = Simple ""
    Simple l `mappend` Simple r = Simple $  l ++ r

infixl 9 !!!
(!!!) :: [a] -> Int -> Except ListIndexError a
(!!!) lst ind | ind < 0 = throwE ErrNegativeIndex
              | ind >= length(take (ind + 1) lst) = throwE $ ErrIndexTooLarge ind
              | otherwise = return $ lst !! ind

lie2se :: ListIndexError -> SimpleError
lie2se (ErrIndexTooLarge i) = Simple $ "[index (" ++ show i ++ ") is too large]"
lie2se ErrNegativeIndex = Simple "[negative index]"


