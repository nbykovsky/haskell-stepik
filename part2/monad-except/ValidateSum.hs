-- task https://stepik.org/lesson/30722/step/14?unit=11809
module ValidateSum where

import Control.Monad.Trans.Except
import Control.Monad
import Control.Applicative

newtype Validate e a = Validate { getValidate :: Either [e] a }

data ReadError = EmptyInput | NoParse String
  deriving Show

data SumError = SumError Integer ReadError
  deriving Show

tryRead :: Read a => String -> Except ReadError a
tryRead [] = throwE EmptyInput
tryRead s =
    case reads s of
        [(a,b)] -> if null b then return a
                                      else throwE $ NoParse s
        _       -> throwE $ NoParse s

instance Functor (Validate e) where
    fmap = liftM

instance Applicative (Validate e) where
    pure = return
    (<*>) = ap

instance Monad (Validate e) where
    return a = Validate (Right a)
    m >>= k =
        case getValidate m of
            Left e -> Validate (Left e)
            Right x -> k x

instance Monoid e => Alternative (Validate e) where
    empty = mzero
    (<|>) = mplus

instance Monoid e => MonadPlus (Validate e) where
    mzero = Validate (Left mempty)
    Validate x `mplus` Validate y = Validate $
        case x of
            Left e -> either (Left . mappend e) Right y
            r      -> r

-- collectE :: Except e a -> Validate e a
-- collectE e = case runExcept e of
--     Right r -> Validate $ Right r
--     Left e' -> Validate $ Left [e']

-- validateSum :: [String] -> Validate SumError Integer
-- validateSum = undefined


validateSum :: [String] -> Validate SumError Integer
validateSum strs = Validate $ foldl folder (Right 0)  (map transformed indexed)
    where indexed = zip [1..] (map tryRead strs :: [Except ReadError Integer])
          transformed (i, r) = runExcept $ withExcept (\x -> [SumError i x]) r
          folder :: Either [SumError] Integer -> Either [SumError] Integer -> Either [SumError] Integer
          folder (Left e') (Left e'') =  Left (e' ++ e'')
          folder _  (Left e) =  Left e
          folder (Left e) _ = Left e
          folder  (Right v1) (Right v2) =  Right (v1 + v2)

-- better solution
-- instance Functor (Validate e) where
--   fmap f = Validate . fmap f . getValidate
--
-- instance Applicative (Validate e) where
--   pure = Validate . pure
--   Validate (Right f) <*> vx = f <$> vx
--   vf <*> Validate (Right x) = ($ x) <$> vf
--   Validate (Left es1) <*> Validate (Left es2) = Validate $ Left (es1 `mappend` es2)
--
-- collectE :: Except e a -> Validate e a
-- collectE ex = case runExcept ex of Right x -> Validate (Right x); Left e -> Validate (Left [e])
--
-- validateSum :: [String] -> Validate SumError Integer
-- validateSum xs = sum <$> traverse (\(i, s) -> collectE $ withExcept (SumError i) $ tryRead s) (zip [1..] xs)