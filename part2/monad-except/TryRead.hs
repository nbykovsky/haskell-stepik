-- task https://stepik.org/lesson/30722/step/8?unit=11809
module TryRead where
import Control.Monad.Trans.Except

data ReadError = EmptyInput | NoParse String
  deriving Show

tryRead :: Read a => String -> Except ReadError a
tryRead [] = throwE EmptyInput
tryRead s =
    case reads s of
        [(a,b)] -> if null b then return a
                                      else throwE $ NoParse s
        _       -> throwE $ NoParse s
