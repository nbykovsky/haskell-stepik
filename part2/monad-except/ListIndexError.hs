-- task https://stepik.org/lesson/30722/step/7?unit=11809
module ListIndexError where

import Control.Monad.Trans.Except

data ListIndexError = ErrIndexTooLarge Int | ErrNegativeIndex
  deriving (Eq, Show)

infixl 9 !!!
(!!!) :: [a] -> Int -> Except ListIndexError a
(!!!) lst ind | ind < 0 = throwE ErrNegativeIndex
              | ind >= length(take (ind + 1) lst) = throwE $ ErrIndexTooLarge ind
              | otherwise = return $ lst !! ind