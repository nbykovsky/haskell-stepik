-- task https://stepik.org/lesson/30722/step/9?unit=11809
module TrySum where


import Control.Monad.Trans.Except

data ReadError = EmptyInput | NoParse String
  deriving Show

data SumError = SumError Integer ReadError
  deriving Show

tryRead :: Read a => String -> Except ReadError a
tryRead [] = throwE EmptyInput
tryRead s =
    case reads s of
        [(a,b)] -> if null b then return a
                                      else throwE $ NoParse s
        _       -> throwE $ NoParse s


trySum :: [String] -> Except SumError Integer
trySum strs = except $ foldl folder (Right 0)  (map transformed indexed)
    where indexed = zip [1..] (map tryRead strs :: [Except ReadError Integer])
          transformed (i, r) = runExcept $ withExcept (SumError i) r
          folder :: Either SumError Integer -> Either SumError Integer -> Either SumError Integer
          folder (Left e) _ =  Left e
          folder _  (Left e) =  Left e
          folder  (Right v1) (Right v2) =  Right (v1 + v2)
