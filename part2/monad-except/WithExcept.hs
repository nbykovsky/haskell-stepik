-- task https://stepik.org/lesson/30722/step/3?unit=11809
module WithExcept where

newtype Except e a = Except {runExcept :: Either e a}
                     deriving Show

except :: Either e a -> Except e a
except = Except

withExcept :: (e -> e') -> Except e a -> Except e' a
withExcept f (Except (Left s)) = Except $ Left $ f s
withExcept _ (Except (Right a)) = Except $ Right a
