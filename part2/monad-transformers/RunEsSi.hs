-- task https://stepik.org/lesson/31556/step/11?unit=11810
module RunEsSi where

import Control.Monad
import Control.Monad.Trans
import Control.Monad.Trans.State
import Control.Monad.Trans.Except

tickCollatz :: State Integer Integer
tickCollatz = do
  n <- get
  let res = if odd n then 3 * n + 1 else n `div` 2
  put res
  return n

type EsSi = ExceptT String (State Integer)

runEsSi :: EsSi a -> Integer -> (Either String a, Integer)
runEsSi = runState . runExceptT


go :: Integer -> Integer -> State Integer Integer -> EsSi ()
go low high s = do
    n <- lift get
    let next =  execState s n
    lift . put $ next
    when (next <= low) $ throwE "Lower bound"
    when (next >= high) $ throwE "Upper bound"
    return ()


-- better solution
-- go :: Integer -> Integer -> State Integer Integer -> EsSi ()
-- go lower upper next = do
--   lift next
--   n <- lift get
--   when (n <= lower) (throwE "Lower bound")
--   when (n >= upper) (throwE "Upper bound")