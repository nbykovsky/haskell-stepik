-- task https://stepik.org/lesson/31556/step/12?unit=11810
module RiiEsSiT where

import Control.Monad
import Control.Monad.Trans
import Control.Monad.Trans.Reader
import Control.Monad.Trans.State
import Control.Monad.Trans.Except

tickCollatz' :: StateT Integer IO Integer
tickCollatz' = do
  n <- get
  let res = if odd n then 3 * n + 1 else n `div` 2
  lift $ putStrLn $ show res
  put res
  return n


type RiiEsSiT m = ReaderT (Integer,Integer) (ExceptT String (StateT Integer m))

-- solution
runRiiEsSiT :: ReaderT (Integer,Integer) (ExceptT String (StateT Integer m)) a
                 -> (Integer,Integer)
                 -> Integer
                 -> m (Either String a, Integer)
runRiiEsSiT mnd bounds =runStateT (runExceptT (runReaderT mnd bounds))

go :: Monad m => StateT Integer m Integer -> RiiEsSiT m ()
go st = do
    _ <- (lift . lift) st
    n <- (lift . lift) get
    (lower, upper) <- ask
    when (n <= lower) ((lift . throwE) "Lower bound")
    when (n >= upper) ((lift . throwE) "Upper bound")