-- task https://stepik.org/lesson/31556/step/10?unit=11810
module VeryComplexComputation where

import Control.Monad.Trans.Reader -- transformers
import Control.Monad.Trans.Writer
import Control.Monad.Trans (lift)
import Data.Char (toUpper)

type MyRWT m = ReaderT [String] (WriterT String m)

runMyRWT :: MyRWT m a -> [String] -> m (a, String)
runMyRWT rw e = runWriterT (runReaderT rw e)

myAsks :: Monad m => ([String] -> a) -> MyRWT m a
myAsks = asks

myTell :: Monad m => String -> MyRWT m ()
myTell = lift . tell

myLift :: Monad m => m a ->  MyRWT m a
myLift = lift . lift


logFirstAndRetSecond :: MyRWT Maybe String
logFirstAndRetSecond = do
  xs <- myAsks id
  case xs of
    (el1 : el2 : _) -> myTell el1 >> return (map toUpper el2)
    _ -> myLift Nothing


-- solution
veryComplexComputation :: MyRWT Maybe (String, String)
veryComplexComputation = do
    xs <- myAsks id
    let o = filter (odd . length) xs
    let e = filter (even . length) xs
    let up = map toUpper
    case (o, e) of
        (o':o'':_, e':e'':_) -> myTell e' >> myTell "," >> myTell o' >> return (up e'', up o'')
        _                        -> myLift Nothing



-- better solution
-- veryComplexComputation :: MyRWT Maybe (String, String)
-- veryComplexComputation = do
--   s1 <- myWithReader (filter $ even . length) logFirstAndRetSecond
--   myTell ","
--   s2 <- myWithReader (filter $ odd  . length) logFirstAndRetSecond
--   return (s1, s2)
--
-- myWithReader :: Monad m => ([String] -> [String]) -> MyRWT m a -> MyRWT m a
-- myWithReader = withReaderT