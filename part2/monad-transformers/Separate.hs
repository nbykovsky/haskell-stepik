-- task https://stepik.org/lesson/31556/step/5?unit=11810
module Separate where

import Control.Monad.Trans.Writer
import Control.Monad.Trans (lift)

separate :: (a -> Bool) -> (a -> Bool) -> [a] -> WriterT [a] (Writer [a]) [a]
separate p1 p2 lst = do
    tell $ filter p1 lst
    lift $ tell (filter p2 lst)
    return $ filter (\e -> not (p1 e || p2 e)) lst
