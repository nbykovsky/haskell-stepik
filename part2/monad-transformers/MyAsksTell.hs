-- task https://stepik.org/lesson/31556/step/7?unit=11810
module MyAsksTell where

import Control.Monad.Trans.Reader
import Control.Monad.Trans.Writer
import Control.Monad.Trans (lift)

type MyRW = ReaderT [String] (Writer String)

myAsks :: ([String] -> a) -> MyRW a
myAsks = asks

myTell :: String -> MyRW ()
myTell = lift . tell