### Monad Transformers

```haskell
import Control.Monad.Trans.Reader -- transformers
import Control.Monad.Trans.Writer
import Control.Monad.Trans (lift)
import Data.Char (toUpper)

```

secondElem returns second element from list from environment using Reader Monad
```haskell
secondElem :: Reader [String] String
secondElem = do
    el2 <- asks (map toUpper . head . tail)
    return el2

demo> strings = ["abc", "defg", "hij"]
demo> runReader secondElem strings
"DEFG"
```

example for Writer Monad
```haskell
logFirst :: [String] -> Writer String String
logFirst xs = do
    let el1 = head xs
    let el2 = (map toUpper . head . tail) xs
    tell el1
    return el2

demo> runWriter (logFirst strings)
("DEFG", "abc")
```

Combination of Reader and Writer
```haskell
logFirstAndRetSecond :: ReaderT [String] -- transformer
                        (Writer String)  -- internal monad
                        String           -- returning type

logFirstAndRetSecond = do
    el1 <- asks head
    el2 <- asks (map toUpper . head . tail)
    lift $ tell el1 -- lift calculations from internal monad
    return el2

demo> :k Writer String
Writer String :: * -> *
demo> :k Reader [String]
Reader [String] :: * -> *
demo> :k ReaderT [String]
Reader [String] :: (k -> *) -> k -> *

demo> :t runReaderT
runReaderT :: ReaderT r m a -> r -> m a
demo> runWriter (runReaderT logFirstAndRetSecond strings)
("DEFG", "abc")


```
runReaderT runs calculation in ReaderT and returns value inside internal monad (m)


simplification of Monad Transformer
```haskell
type MyRW = ReaderT [String] (Writer String)

logFirstAndRetSecond :: MyRW String

runMyRW :: MyRW a -> [Strings] -> (a, String)
runMyRW rw e = runWriter (runReaderT rw e)


demo> :k MyRW
MyRw :: * -> *

demo> runMyRW logFirstAndRetSecond strings
("DEFG", "abc")

```

All the monads in Control.Monad.Trans and Transformers based on Identity monad
```haskell
demo> :i Reader
type Reader r = ReaderT r Data.Functor.Identity.Identity :: * -> *
demo> :i reader
reader :: Monad m => (r -> a) -> ReaderT r m a

```