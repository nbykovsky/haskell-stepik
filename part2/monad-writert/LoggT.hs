-- task https://stepik.org/lesson/38578/step/7?unit=20503
module LoggT where

import Control.Monad
import Control.Monad.Identity

data Logged a = Logged String a deriving (Eq,Show)

newtype LoggT m a = LoggT { runLoggT :: m (Logged a) }

logTst :: LoggT Identity Integer
logTst = do
  x <- LoggT $ Identity $ Logged "AAA" 30
  y <- return 10
  z <- LoggT $ Identity $ Logged "BBB" 2
  return $ x + y + z

failTst :: [Integer] -> LoggT [] Integer
failTst xs = do
  5 <- LoggT $ fmap (Logged "") xs
  LoggT [Logged "A" ()]
  return 42

instance Monad m => Functor (LoggT m) where
    fmap = liftM

instance Monad m => Applicative (LoggT m) where
    pure = return
    (<*>) = ap

instance Monad m => Monad (LoggT m) where
  return x = LoggT $ return $ Logged "" x
  m >>= k  = LoggT $ do
    (Logged s v)   <- runLoggT m
    (Logged s' v') <- runLoggT (k v)
    return $ Logged (s ++ s') v'
  fail = LoggT . fail



-- instance (Monoid w, Monad m) => Monad (WriterT w m) where
--     (>>=) :: WriterT w m a -> (a -> WriterT w m b) -> WriterT w m b
--     m >>= k = WriterT $ do
--         ~(v, w) <- runWriterT m
--         ~(v', w') <- runWriterT (k v)
--         return (v', w `mappend` w')
--     fail :: String Writer w m a
--     -- internal monad might know how to deal with errors
--     fail = WriterT . fail