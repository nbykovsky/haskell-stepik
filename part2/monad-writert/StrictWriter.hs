{-# LANGUAGE InstanceSigs #-}

module StrictWriter where

import Control.Applicative

newtype Writer w a = Writer {runWriter :: (a, w)}

newtype WriterT w m a = WriterT {runWriterT :: m (a, w)}

instance Functor (Writer w) where
    fmap :: (a -> b) -> Writer w a -> Writer w b
    fmap f = Writer . updater . runWriter
        where updater ~(x, l) = (f x, l)

instance Functor m => Functor (WriterT w m) where
    fmap :: (a -> b) -> WriterT w m a -> WriterT w m b
    fmap f = WriterT . fmap updater .runWriterT
        where updater ~(x, l) = (f x, l)

instance Monoid w => Applicative (Writer w) where
    pure :: a -> Writer w a
    pure x = Writer (x, mempty)
    (<*>) :: Writer w (a -> b) -> Writer w a -> Writer w b
    f <*> v = Writer $ updater (runWriter f) (runWriter v)
        where updater ~(g, w) ~(x, w') = (g x, w `mappend` w')

instance (Monoid w, Applicative m) => Applicative (WriterT w m) where
    pure :: a -> WriterT w m a
    pure x = WriterT $ pure (x, mempty)
    (<*>) :: WriterT w m (a -> b) -> WriterT w m a -> WriterT w m b
    f <*> v = WriterT $ liftA2 updater (runWriterT f) (runWriterT v)
        where updater ~(g, w) ~(x, w') = (g x, w `mappend` w')


newtype StrictWriter w a = StrictWriter { runStrictWriter :: (a, w) }

instance Functor (StrictWriter w) where
  fmap f  = StrictWriter . updater . runStrictWriter
    where updater (x, l) = (f x, l)

instance Monoid w => Applicative (StrictWriter w) where
  pure x  = StrictWriter (x, mempty)

  f <*> v = StrictWriter $ updater (runStrictWriter f) (runStrictWriter v)
    where updater (g, w) (x, w') = (g x, w `mappend` w')

actionLazy = Writer (42,"Hello!")
actionStrict = StrictWriter (42,"Hello!")