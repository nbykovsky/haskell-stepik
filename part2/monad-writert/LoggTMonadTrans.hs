-- task https://stepik.org/lesson/38578/step/12?unit=20503
module LoggTMonadTrans where


import Control.Monad
import Control.Monad.Identity
import Control.Monad.Trans

data Logged a = Logged String a deriving (Eq,Show)

newtype LoggT m a = LoggT { runLoggT :: m (Logged a) }

instance Monad m => Functor (LoggT m) where
    fmap = liftM

instance Monad m => Applicative (LoggT m) where
    pure = return
    (<*>) = ap

instance Monad m => Monad (LoggT m) where
  return x = LoggT $ return $ Logged "" x
  m >>= k  = LoggT $ do
    (Logged s v)   <- runLoggT m
    (Logged s' v') <- runLoggT (k v)
    return $ Logged (s ++ s') v'
  fail = LoggT . fail

write2log :: Monad m => String -> LoggT m ()
write2log s = LoggT . return $ Logged s ()

type Logg = LoggT Identity

runLogg :: Logg a -> Logged a
runLogg = runIdentity . runLoggT

instance MonadTrans LoggT where
  lift m = LoggT $ do
    x <- m
    return $ Logged "" x
