-- task https://stepik.org/lesson/38577/step/16?unit=17396
module Arr2TMonadTrans where
{-# LANGUAGE FlexibleContexts #-}


class MonadTrans t where
  lift :: Monad m => m a -> t m a

newtype Arr2T e1 e2 m a = Arr2T { getArr2T :: e1 -> e2 -> m a }

instance Functor m => Functor (Arr2T e1 e2 m) where
    fmap f arr =  Arr2T $ \p1 p2 -> f <$> getArr2T arr p1 p2


instance Applicative m => Applicative (Arr2T e1 e2 m) where
    pure = Arr2T . const . const . pure
    f <*> v = Arr2T $ \e1 e2 -> getArr2T f e1 e2 <*> getArr2T v e1 e2


instance Monad m => Monad (Arr2T e1 e2 m) where
    m >>= k = Arr2T $ \e1 e2 -> do
        v <- getArr2T m e1 e2
        getArr2T (k v) e1 e2

instance MonadTrans (Arr2T e1 e2) where
--     lift :: Monad m => m a -> MonadTrans r m a
    lift m = Arr2T $ \_ _ -> m

asks2 :: Monad m => (e1 -> e2 -> a) -> Arr2T e1 e2 m a
asks2 f = Arr2T $ \e1 e2 -> return $ f e1 e2

