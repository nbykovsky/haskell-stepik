-- task https://stepik.org/lesson/38577/step/12?unit=17396
module Arr2Arr3Monad where

newtype Arr2T e1 e2 m a = Arr2T { getArr2T :: e1 -> e2 -> m a }
newtype Arr3T e1 e2 e3 m a = Arr3T { getArr3T :: e1 -> e2 -> e3 -> m a }

instance Functor m => Functor (Arr2T e1 e2 m) where
    fmap f arr =  Arr2T $ \p1 p2 -> f <$> getArr2T arr p1 p2

instance Functor m => Functor (Arr3T e1 e2 e3 m) where
    fmap f arr =  Arr3T $ \p1 p2 p3 -> f <$> getArr3T arr p1 p2 p3

instance Applicative m => Applicative (Arr2T e1 e2 m) where
    pure = Arr2T . const . const . pure
    f <*> v = Arr2T $ \e1 e2 -> getArr2T f e1 e2 <*> getArr2T v e1 e2

instance Applicative m => Applicative (Arr3T e1 e2 e3 m) where
    pure = Arr3T . const . const . const . pure
    f <*> v = Arr3T $ \e1 e2 e3-> getArr3T f e1 e2 e3 <*> getArr3T v e1 e2 e3

instance Monad m => Monad (Arr2T e1 e2 m) where
    m >>= k = Arr2T $ \e1 e2 -> do
        v <- getArr2T m e1 e2
        getArr2T (k v) e1 e2

instance Monad m => Monad (Arr3T e1 e2 e3 m) where
    m >>= k = Arr3T $ \e1 e2 e3-> do
        v <- getArr3T m e1 e2 e3
        getArr3T (k v) e1 e2 e3


-- instance Monad m => Monad (ReaderT r m) where
--     return = pure
--     (>>=) :: ReaderT r m a -> (a -> ReaderT r m b) -> ReaderT r m b
--     m >>= k = ReaderT $ \env -> do
--         v <- runReaderT m env
--         runReader (k v) env