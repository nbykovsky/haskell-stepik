-- task https://stepik.org/lesson/38577/step/6?unit=17396
module Arr2Arr3Functor where

newtype Arr2T e1 e2 m a = Arr2T { getArr2T :: e1 -> e2 -> m a }
newtype Arr3T e1 e2 e3 m a = Arr3T { getArr3T :: e1 -> e2 -> e3 -> m a }


-- solution
instance Functor m => Functor (Arr2T e1 e2 m) where
    fmap f arr =  Arr2T $ \p1 p2 -> f <$> getArr2T arr p1 p2

instance Functor m => Functor (Arr3T e1 e2 e3 m) where
    fmap f arr =  Arr3T $ \p1 p2 p3 -> f <$> getArr3T arr p1 p2 p3

-- better solution
-- instance Functor m => Functor (Arr2T e1 e2 m) where fmap f = Arr2T.((fmap f.).).getArr2T
-- instance Functor m => Functor (Arr3T e1 e2 e3 m) where fmap f = Arr3T.(((fmap f.).).).getArr3T