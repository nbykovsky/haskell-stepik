-- task https://stepik.org/lesson/38577/step/13?unit=17396
module Arr3Tfail where

newtype Arr3T e1 e2 e3 m a = Arr3T { getArr3T :: e1 -> e2 -> e3 -> m a }

instance Functor m => Functor (Arr3T e1 e2 e3 m) where
    fmap f arr =  Arr3T $ \p1 p2 p3 -> f <$> getArr3T arr p1 p2 p3

instance Applicative m => Applicative (Arr3T e1 e2 e3 m) where
    pure = Arr3T . const . const . const . pure
    f <*> v = Arr3T $ \e1 e2 e3-> getArr3T f e1 e2 e3 <*> getArr3T v e1 e2 e3

-- solution
instance Monad m => Monad (Arr3T e1 e2 e3 m) where
    m >>= k = Arr3T $ \e1 e2 e3-> do
        v <- getArr3T m e1 e2 e3
        getArr3T (k v) e1 e2 e3
    fail s = Arr3T $ \_ _ _ -> fail s
