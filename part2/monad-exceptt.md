### Monad ExceptT

```haskell
newtype Except e a = Except {runExcept :: Either e a}

newtype Except e m a = ExceptT {runExceptT :: m (Either e a)}

except :: Monad m => Either e a -> ExceptT e m a
except = ExceptT . retur

demo> runExceptT (except $ Right 42) :: [Either String Int]
[Right 42]

```

ExceptT as Functor
```haskell
instance Functor (Except e) where
    fmap :: (a -> b) -> Except e a -> Except e b
    fmap f = Except . fmap f . runExcept

instance Functor m => Functor (ExceptT e m) where    
    fmap :: (a -> b) -> Except e m a -? ExceptT e m b
    fmap f = ExceptT . fmap (fmap f) . runExceptT
    
demo> runExcept (fmap (+9) $ except $ Rignt 42) :: [Either String Int]
[Right 51]

```

ExceptT as Applicative (this implementation has some cons)
```haskell
instance Applicative (Except e) where
    pure = Except . Right
    f <*> v = Excpet $  (runExcept f) <*> (runExcept v) where
        
--    f <*> v = Excpet $ updater (runExcept f) (runExcept v) where
--            updater (Left e) _ = Left e 
--            update (Right g) x = fmap g x
        
instance Applicative m => Applicative (ExeptT e m) where
    pure = ExcpetT . pure . Right
    
    f <*> v = ExceptT $ liftA2 (<*>) (runExcpetT f) (runExcept v) where
    
--    f <*> v = ExceptT $ liftA2 updater (runExcpetT f) (runExcept v) where
--        updater (Left e) _ = Left e 
--        updater (Right g) x = fmap g x

demo> runExcpetT $ (except $ Right (^2)) <*> (except $ Right 3) :: [Either String Int]
[Right 9]

demo> runExcpetT $ (except $ Left "ABC") <*> (except $ Right 3) :: [Either String Int]
[Left "ABC"]

```

ExceptT as Monad
```haskell

instance Monad (Except e) where
    m >>= k = Except $
        case runExcept m of 
            Left e -> Left e
            Right x -> runExcept (k x)
            
instance (Monad m) => Monad (ExceptT e m) where
    m >>= k = ExceptT $ do
        a < runExcept m 
        case a of 
            Left e -> return (Left e)
            Right x -> runExceptT (k x)
            
    fail = ExceptT . fail

demo> runExceptT $ do {f <- except $ Right (^2); return $ f 3} :: [Either String Int]
[Right 9]

```

Except as MonadTrans
```haskell
instance MonadTrans (ExceptT e) where
    lift :: m a -> ExceptT e m a
    lidt = ExceptT . fmap Right

throwE :: Monad m => e -> ExceptT e m a 
throwE = ExceptT . return . Left

catchE :: Monad m => ExceptT e m a -> (e -> ExceptT e' m a) -> ExceptT e' m a
m `catchE` h = ExceptT $ do
    a <- runExceptT m 
    case a of 
        Left l -> runExcept (h l)
        Right r -> return (Right r)
```

Example of lift
```haskell
test f = runIdentity (runStateT (runExceptT f) 3)
demo>test $ (except $ Right 42) >> lift (put 5)
(Right (), 5)
demo>test $ (except $ Right 42) >> lift (modify (+1))
(Right (), 4)
```
Example of throwE
```haskell
demo> test $ throw "Error!" >> lift (mofify (+1))
(Left "Error!", 3)
demo> test $ lift (modify (+2)) >> throw "Error!" >> lift (mofify (+1))
(Left "Error!", 5)

-- same calculation in App (result supposed to be the same)
demo> test $ lift (modify (+2)) *> throw "Error!" *> lift (mofify (+1))
(Left "Error!", 6)
--result is different because implementation of Applicative is not correct

```

Better implementation of Applicative
```haskell
--instance Applicative m => Applicative (ExeptT e m) where
--    pure = ExcpetT . pure . Right   
--    f <*> v = ExceptT $ liftA2 (<*>) (runExcpetT f) (runExcept v) where

instance Monad m => Applicative (ExceptT e m) where
    pure x = ExceptT $ pure (Right x)
    
    ExceptT mef <*> ExceptT mea = ExceptT $ do
        ef <- mef
        case ef of 
            Left e -> return (Left e)
            Right f -> fmap (fmap f) mea

demo> test $ lift (modify (+2)) *> throw "Error!" *> lift (mofify (+1))
(Left "Error!", 5)
-- works as expected

```
